#include "BankAccount.h"
class CurrentAccount :
	public BankAccount
{
public:
	CurrentAccount::CurrentAccount();
	CurrentAccount(const string& typ, const string& acctNum, const string&sCode,
		const Date& cD, double b, const TransactionList& trList, double limit);
	const double getOverDraftLimit() const;
	double CurrentAccount::maxWithdrawalAllowed() const;
	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;
protected:
	double overdraftLimit;

};
