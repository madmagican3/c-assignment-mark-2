//Pascale Vacher - March 16
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Member names, students numbers and courses:

#include "UserInterface.h" 

//---------------------------------------------------------------------------
//UserInterface: class implementation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------

void UserInterface::wait() const {
	char ch;
	cout << "\n\nPress RETURN to go back to menu\n";
	cin.get(ch);
	cin.get(ch);
	//	flushall();
}
int UserInterface::readInCardIdentificationCommand() const {
	showCardIdentificationMenu();
	return (readInCommand());
}
void UserInterface::showCardIdentificationMenu() const {
	cout << "\n\n\n      ________________________________________";
	cout << "\n      _______CARD IDENTIFICATION MENU________";
	cout << "\n      ________________________________________";
	cout << "\n       0           Quit CashPoint application";
	cout << "\n       1              Enter your card details";
	cout << "\n      ________________________________________";
}
int UserInterface::readInAccountProcessingCommand() const{
	showAccountProcessingMenu();
	return (readInCommand());
}
void UserInterface::showAccountProcessingMenu() const {
	cout << "\n\n\n      ________________________________________";
	cout << "\n      ________ACCOUNT PROCESSING MENU________";
	cout << "\n      ________________________________________";
	cout << "\n       0 End account processing & remove card";
	cout << "\n       1                      Display balance";
	cout << "\n       2                Withdraw from account";
	cout << "\n       3                 Deposit into account";
	cout << "\n       4                       Show statement";
	cout << "\n       5                    Show all deposits  //TO BE IMPLEMENTED FOR Task 1c Preparation";
	cout << "\n       6                  Show mini statement  //TO BE IMPLEMENTED FOR Task 1c";
	cout << "\n       7                  Search Transactions  //TO BE IMPLEMENTED FOR Task 1c";
	cout << "\n       8    Clear all transactions up to date  //TO BE IMPLEMENTED FOR Task 1c";
	cout << "\n       9                 Show Funds Available";
	cout << "\n       10         Transfer to another account  ";
	cout << "\n         ________________________________________";
}

const string UserInterface::readInCardToBeProcessed(string& cardNumber) const {
	cout << "\n SELECT THE CARD ...\n";
	cout << "   CARD NUMBER:  ";         //ask for card number
	cin >> cardNumber;
	cout << "\n=========================================";
	//create card file name
	return(FILEPATH + "card_" + cardNumber + ".txt");
}

void UserInterface::showValidateCardOnScreen(int validCode, const string& cardNumber) const {
	switch (validCode)
	{
	case VALID_CARD:		//card valid: it exists, is accessible with that card (and not already open: TO BE IMPLEMENTED)
		cout << "\nTHE CARD (NUMBER: " << cardNumber << ") EXIST!";
		break;
	case UNKNOWN_CARD:		//card does not exist
		cout << "\nERROR: INVALID CARD\n"
			<< "\nTHE CARD (NUMBER: " << cardNumber << ") DOES NOT EXIST!";
		break;
	case EMPTY_CARD:		//account exists but is not accessible with that card
		cout << "\nERROR: EMPTY CARD"
			<< "\nTHE CARD (NUMBER: " << cardNumber << ") DOES NOT LINK TO ANY ACCOUNT!";
		break;
	}
}
void UserInterface::showTotal(double total) const{
	cout << "The total money in all your bank accounts is " << total;
}
void UserInterface::showCardOnScreen(const string& cardDetails) const {
	cout << "\n=========================================";
	cout << "\n________ CARD DETAILS ___________________";
	cout << cardDetails;
	cout << "\n________ END CARD DETAILS _______________";
	cout << "\n=========================================";
}

const string UserInterface::readInAccountToBeProcessed(string& accountNumber, string& sortCode) const {
	cout << "\n SELECT THE ACCOUNT ...\n";
	cout << "   ACCOUNT NUMBER:  ";	//ask for account number
	cin >> accountNumber;
	cout << "   SORT CODE:       ";	//ask for sort code
	cin >> sortCode;
	cout << "\n=========================================";
	//create account file name
	return(FILEPATH + "account_" + accountNumber + "_" + sortCode + ".txt");
}

void UserInterface::showValidateAccountOnScreen(int validCode, const string& accNum, const string& sortCode) const {
	switch (validCode)
	{
	case VALID_ACCOUNT:		//account valid: it exists, is accessible with that card (and not already open: TO BE IMPLEMENTED)
		cout << "\nTHE ACCOUNT (NUMBER: " << accNum
			<< " CODE: " << sortCode << ") IS NOW OPEN!";
		break;
	case UNKNOWN_ACCOUNT:		//account does not exist
		cout << "\nERROR: INVALID ACCOUNT"
			<< "\nTHE ACCOUNT (NUMBER: " << accNum
			<< " CODE: " << sortCode << ") DOES NOT EXIST!";
		break;
	case UNACCESSIBLE_ACCOUNT:		//account exists but is not accessible with that card
		cout << "\nERROR: INVALID ACCOUNT"
			<< "\nTHE ACCOUNT (NUMBER: " << accNum
			<< " CODE: " << sortCode << ") IS NOT ACCESSIBLE WITH THIS CARD!";
		break;
	case DUPLICATE_ACCOUNT:
		cout << "\nERROR: YOU'RE ATTEMPTING TO TRANSFER MONEY TO THE SAME ACCOUNT"
			<< "\nTHE ACCOUNT NUMBER: " << accNum
			<< " CODE: " << sortCode << ") IS THE SAME ACCOUNT";
	}
}

//input functions

double UserInterface::readInWithdrawalAmount() const {
	//ask for the amount to withdraw
	cout << "\nAMOUNT TO WITHDRAW: \234";
	return (readInPositiveAmount());
}
double UserInterface::readInDepositAmount() const {
	//ask for the amount to deposit
	cout << "\nAMOUNT TO DEPOSIT: \234";
	return (readInPositiveAmount());
}
double UserInterface::readInTransferAmount() const {
	//ask for the amount to withdraw
	cout << "\nAMOUNT TO WITHDRAW: \234";
	return (readInPositiveAmount());
}

//output functions

void UserInterface::showProduceBalanceOnScreen(double balance) const {
	cout << fixed << setprecision(2) << setfill(' ');
	cout << "\nTHE CURRENT BALANCE IS: \234" << setw(12) << balance;//display balance
}
void UserInterface::showWithdrawalOnScreen(bool trAuthorised, double withdrawnAmount) const {
	if (trAuthorised)
		cout << "\nTRANSACTION AUTHORISED!: \n\234"
		<< setw(0) << withdrawnAmount
		<< " WITHDRAWN FROM ACCOUNT";
	else //not enough money
		cout << "\nTRANSACTION IMPOSSIBLE!";
}
void UserInterface::showDepositOnScreen(bool trAuthorised, double depositAmount) const {
	if (trAuthorised)
		cout << "\nTRANSACTION AUTHORISED!:\n\234"
		<< setw(0) << depositAmount
		<< " DEPOSITED INTO ACCOUNT";
	else //too much to deposit
		cout << "\nTRANSACTION IMPOSSIBLE!";
}
void UserInterface::showStatementOnScreen(const string& statement) const {
	cout << "\nPREPARING STATEMENT...";
	cout << "\n________ ACCOUNT STATEMENT _____";
	cout << statement;
	cout << "\n________ END ACCOUNT STATEMENT _____";
}

void UserInterface::chooseAccount()const{
	cout << "\nPlease choose which account you want to transfer money too";
}
bool UserInterface::validateTransferSelection(string accountNo, string sortCode)const{
	string input;
	cout << "\nTHE ACCOUNT (NUMBER:" << accountNo << " CODE:" << sortCode << " IS NOW OPEN";
	do{
		cout << "\nARE YOU SURE YOU WANT TO SEND MONEY TO THIS ACCOUNT? y/n";
		cin >> input;
		cout << input;

	} while (!(input == "y" || input == "n"));
	return input == "y";
}
//---------------------------------------------------------------------------
//private support member functions
//---------------------------------------------------------------------------

void UserInterface::showWelcomeScreen() const {
	cout << "\n\n\n             _____WELCOME TO THE ATM_____";
}
void UserInterface::showByeScreen() const {
	cout << "\n\n\n________________NEXT CUSTOMER...\n\n";
}
int UserInterface::readInCommand() const{
	int com;
	cout << "\n          ENTER YOUR COMMAND: ";
	cin >> com;
	return com;
}
void UserInterface::showErrorInvalidCommand() const {
	cout << "\nINVALID COMMAND CHOICE, TRY AGAIN";
}
double UserInterface::readInPositiveAmount() const {
	double amount;
	cin >> amount;
	while (amount <= 0.0)
	{
		cout << "\nAMOUNT SHOULD BE A POSITIVE AMOUNT, TRY AGAIN: ";
		cin >> amount;
	}
	return amount;
}