#include "SavingAccount.h"
class ChildAccount :
	public SavingAccount
{
public:
	ChildAccount();
	ChildAccount(const string& typ, const string& acctNum, const string&sCode,
		const Date& cD, double b, const TransactionList& trList, double min, double minPaidIn, double maxPaidIn);
	double ChildAccount::getMaxBalance();
	virtual void ChildAccount::recordDeposit(double amountToDeposit);
	virtual bool ChildAccount::canWithdraw();
	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;
	double ChildAccount::getMinBalance();
protected:
	double maxBalance;
	double minBalance;

};