#include "ISA.h"
ISA::ISA(){
}
ISA::ISA(const string& typ, const string& acctNum, const string&sCode,
	const Date& cD, double b, const TransactionList& trList, double max, double current,
	Date endDeposit, double min) :SavingAccount(typ, acctNum, sCode, cD, b, trList, min){
	endDepositPeriod = endDeposit;
	maxDeposit = max;
	currentDeposit = current;
}
Date ISA::getEndDepositPeriod() const{
	return endDepositPeriod;
}
double ISA::getCurrentDeposit()const{
	return currentDeposit;
}
double ISA::getMaxDeposit()const{
	return maxDeposit;
}
bool  ISA::canDeposit(double amountToWithdraw){
	if (((currentDeposit + amountToWithdraw) > maxDeposit)&&(BankAccount::getCreationDate().getYear() >= Date::currentDate().getYear())){
		return true;
	}
	return false;
}
void ISA::recordDeposit(double amountToDeposit){
	//create a deposit transaction
	if (canDeposit(amountToDeposit)){
		Transaction aTransaction("deposit_to_ATM", amountToDeposit);
		//update active bankaccount
		TransactionList transactions_ = BankAccount::getTransactions();
		transactions_.addNewTransaction(aTransaction);		//update transactions_
		BankAccount::updateBalance(amountToDeposit);			//increase balance_
	}
}
istream& ISA::getAccountDataFromStream(istream& is) {
	//get BankAccount details from stream
	SavingAccount::getAccountDataFromStream(is);
	is >> maxDeposit;
	is >> currentDeposit;
	is >> endDepositPeriod;
	return is;
}
ostream& ISA::putAccountDetailsInStream(ostream& os) const {
	SavingAccount::putAccountDetailsInStream(os);
	//put (unformatted) BankAccount details in stream
	os << maxDeposit;
	os << currentDeposit;
	os << endDepositPeriod;
	return os;
}

