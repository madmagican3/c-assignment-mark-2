#include "ChildAccount.h"

ChildAccount::ChildAccount() :SavingAccount(){
}

ChildAccount::ChildAccount(const string& typ, const string& acctNum, const string& sCode,
	const Date& cD, double b,
	const TransactionList& trList, double min,double minPaidIn ,double maxPaidIn) : SavingAccount(typ, acctNum, sCode, cD, b, trList,min){
	maxBalance = maxPaidIn;
	minBalance = minPaidIn;
}
double ChildAccount::getMaxBalance(){
	return maxBalance;
}
double ChildAccount::getMinBalance(){
	return minBalance;
}
void ChildAccount::recordDeposit(double amountToDeposit){
	//create a deposit transaction
	if (amountToDeposit + BankAccount::getBalance() <= maxBalance&&amountToDeposit >=minBalance &&amountToDeposit <=maxBalance){
		Transaction aTransaction("deposit_to_ATM", amountToDeposit);
		//update active bankaccount
		TransactionList transactions_ = BankAccount::getTransactions();
		transactions_.addNewTransaction(aTransaction);		//update transactions_
		updateBalance(amountToDeposit);			//increase balance_
	}
}
bool ChildAccount::canWithdraw(){
	return false;
}
istream& ChildAccount::getAccountDataFromStream(istream& is) {
	SavingAccount::getAccountDataFromStream(is);
	//get BankAccount details from stream
	is >> minBalance;
	is >> maxBalance;
	return is;
}
ostream& ChildAccount::putAccountDetailsInStream(ostream& os) const {
	SavingAccount::putAccountDetailsInStream(os);
	//put (unformatted) BankAccount details in stream
	os << minBalance; 
	os << maxBalance;
	return os;
}
