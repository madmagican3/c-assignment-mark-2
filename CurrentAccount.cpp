#include "CurrentAccount.h"

CurrentAccount::CurrentAccount(){};
CurrentAccount::CurrentAccount(const string& typ, const string& acctNum, const string& sCode,
	const Date& cD, double b,
	const TransactionList& trList, double limit) :BankAccount (typ,acctNum,sCode,cD,b,trList){
	overdraftLimit = limit;
}
const double CurrentAccount::getOverDraftLimit() const{
	return overdraftLimit;
}
double CurrentAccount::maxWithdrawalAllowed() const {
	//return borrowable amount
	return BankAccount::getBalance() + overdraftLimit;
}
istream& CurrentAccount::getAccountDataFromStream(istream& is) {
	//get BankAccount details from stream
	BankAccount::getAccountDataFromStream(is);
	is >> overdraftLimit;
	return is;
}
ostream& CurrentAccount::putAccountDetailsInStream(ostream& os) const {
	BankAccount::putAccountDetailsInStream(os);
	//put (unformatted) BankAccount details in stream
	os << overdraftLimit;
	return os;
}
