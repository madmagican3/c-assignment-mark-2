//Pascale Vacher - March 16
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Member names, students numbers and courses:

#ifndef ConstantsH
#define ConstantsH

#include <string>
using namespace std;

//menu command
const int QUIT_COMMAND(0);

//account type
const int BANKACCOUNT_TYPE(100000);
const int CURRENT(1);
const int BANK(0);
const int SAVINGS(2);
const int ISAA(4);
const int CHILD(3);

//card state
const int VALID_CARD(0);
const int UNKNOWN_CARD(1);
const int EMPTY_CARD(2);

//account state
const int VALID_ACCOUNT(0);
const int UNKNOWN_ACCOUNT(1);
const int UNACCESSIBLE_ACCOUNT(2);
const int DUPLICATE_ACCOUNT(3);
//ressource path
const string FILEPATH("data\\");

#endif