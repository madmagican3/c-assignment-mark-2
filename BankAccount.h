//Pascale Vacher - March 16
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Member names, students numbers and courses:

#ifndef BankAccountH
#define BankAccountH

//---------------------------------------------------------------------------
//BankAccount: class declaration
//---------------------------------------------------------------------------

//#include "Date.h"
//#include "Transaction.h"
#include "TransactionList.h"

#include <fstream>
using namespace std;


class BankAccount {
public:
    //constructors & destructor
	BankAccount();
    BankAccount(const string& typ, const string& acctNum, const string& sCode,
                          const Date& cD, double b,
                          const TransactionList& trList);
    virtual ~BankAccount();

	//getter (assessor) functions
	const string getAccountType() const;
    const string getAccountNumber() const;
    const string getSortCode() const;
    const Date getCreationDate() const;
	double getBalance() const;
    const TransactionList getTransactions() const;
    bool	isEmptyTransactionList() const;

	//other operations
	const string prepareFormattedStatement() const;

    virtual void recordDeposit(double amount);

	virtual double maxWithdrawalAllowed() const;
	virtual bool canWithdraw(double amount) const;
    void recordWithdrawal(double amount);

	void readInBankAccountFromFile(const string& fileName);
	void storeBankAccountInFile(const string& fileName) const;
	//functions to put data into and get data from streams
	ostream& putDataInStream(ostream& os) const;
	virtual ostream& putAccountDetailsInStream(ostream& os) const;
	istream& getDataFromStream(istream& is);
	virtual istream& getAccountDataFromStream(istream& is);
	virtual bool BankAccount::canDeposit();
	void BankAccount::recordTransfer(double transferAmount, string recievingGiving, string sortCode, string accountNo);

	const string prepareFormattedAccountDetails() const;
	const string prepareFormattedTransactionList() const;
protected:
	void updateBalance(double amount);
	string accountType_;
	string accountNumber_;
	string sortCode_;
	Date   creationDate_;
	double balance_;
	TransactionList transactions_;
private:
    //data items
    
 
	//support functions
};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const BankAccount&);	//output operator
istream& operator>>(istream&, BankAccount&);	    //input operator

#endif
