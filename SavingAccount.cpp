#include "SavingAccount.h"
SavingAccount::SavingAccount() :BankAccount(){}

SavingAccount::SavingAccount(const string& typ, const string& acctNum, const string& sCode,
	const Date& cD, double b,
	const TransactionList& trList, double min) : BankAccount(typ, acctNum, sCode, cD, b, trList){
	minBalance = min;
}
double SavingAccount::getMinBalance(){
	return minBalance;
}
double SavingAccount::maxWithdrawalAllowed() const {
	//return borrowable amount
	return BankAccount::getBalance() - minBalance;
}
istream& SavingAccount::getAccountDataFromStream(istream& is) {
	BankAccount::getAccountDataFromStream(is);
	//get BankAccount details from stream
	is >> minBalance;
	return is;
}
ostream& SavingAccount::putAccountDetailsInStream(ostream& os) const {
	BankAccount::putAccountDetailsInStream(os);
	//put (unformatted) BankAccount details in stream
	os << minBalance;
	return os;
}
