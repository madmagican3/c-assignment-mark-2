#ifndef SavingAccountH
#define SavingAccountH
#include "BankAccount.h"


class SavingAccount :
	public BankAccount
{
public:
	SavingAccount();
	SavingAccount(const string& typ, const string& acctNum, const string&sCode,
		const Date& cD, double b, const TransactionList& trList, double min);
	virtual	double SavingAccount::maxWithdrawalAllowed() const;
	double SavingAccount::getMinBalance();
	virtual ostream& putAccountDetailsInStream(ostream& os) const;
	virtual istream& getAccountDataFromStream(istream& is);
protected:
	double minBalance;


};

#endif