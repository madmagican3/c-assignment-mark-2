#include "SavingAccount.h"
class ISA :
	public SavingAccount
{
public:
	ISA();
	ISA(const string& typ, const string& acctNum, const string&sCode,
		const Date& cD, double b, const TransactionList& trList, double maxDeposit, double currentDeposit, 
		Date endDepositPeriod, double min);
	Date ISA::getEndDepositPeriod() const;
	double ISA::getCurrentDeposit()const;
	double ISA::getMaxDeposit()const;
	virtual void ISA::recordDeposit(double amountToDeposit);
	virtual bool ISA::canDeposit(double amountToWithdraw);
	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;
protected:
	double maxDeposit,currentDeposit;
	Date endDepositPeriod;
};