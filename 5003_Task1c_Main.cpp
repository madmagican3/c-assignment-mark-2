//Pascale Vacher - March 16
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Member names, students numbers and courses:

#include "CashPoint.h"         //include modules header files

//---------------------------------------------------------------------------

//main application

int main() { 
	//create the application
	CashPoint theCashPoint;
    //run it
	theCashPoint.activateCashPoint();
	//destroy it
	//destructor called here
    return 0;
}
